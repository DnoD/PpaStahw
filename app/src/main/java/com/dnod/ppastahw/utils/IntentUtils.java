package com.dnod.ppastahw.utils;


import android.content.Intent;
import android.net.Uri;

import com.dnod.ppastahw.AppController;
import com.dnod.ppastahw.R;
import com.google.android.gms.maps.model.LatLng;

public final class IntentUtils {

    public static Intent createImagePickIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        return Intent.createChooser(intent,
                AppController.getInstance().getString(R.string.title_image_pick));
    }

    public static Intent createGoogleMpasIntent(LatLng place) {
        return new Intent(Intent.ACTION_VIEW,
                Uri.parse("geo:0,0?q=" + place.latitude + "," + place.longitude + ""));
    }
}
