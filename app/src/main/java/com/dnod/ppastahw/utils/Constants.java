package com.dnod.ppastahw.utils;


public interface Constants {
    int PLACE_PICKER_REQUEST = 1;
    int FINE_LOCATION_PERMISSION_REQUEST = 2;
    int PICK_IMAGE_REQUEST = 3;
    int READ_STORAGE_PERMISSION_REQUEST = 4;
}
