package com.dnod.ppastahw.service;


import com.dnod.ppastahw.data.ChatMessage;

import java.util.List;

public interface IDataStorage {

    void saveChatHistory(List<ChatMessage> history);

    List<ChatMessage> getChatHistory(String userId);

    void saveMessage(ChatMessage message);

    void registerDataObserver(String userId, Observer observer);

    void unregisterDataObserver(Observer observer);

    interface Observer {
        void onNewMessage(ChatMessage message);
    }
}
