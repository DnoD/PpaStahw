package com.dnod.ppastahw.service.impl.storage;

import com.dnod.ppastahw.data.ChatMessage;
import com.dnod.ppastahw.service.Marshaller;
import com.google.android.gms.maps.model.LatLng;

public class ChatMessageMarshaller extends Marshaller<RealmChatMessage, ChatMessage> {

    private static final ChatMessageMarshaller instance = new ChatMessageMarshaller();

    public static ChatMessageMarshaller getInstance() {
        return instance;
    }

    @Override
    public ChatMessage fromEntity(RealmChatMessage entity) {
        return new ChatMessage().setId(entity.getId()).setText(entity.getText())
                .setCreatedTime(entity.getCreatedTime())
                .setReceiver(entity.getReceiver())
                .setLocalImagePath(entity.getLocalImagePath())
                .setSender(entity.getSender())
                .setPlace(entity.getLocationLat() != null ?
                        new LatLng(entity.getLocationLat(), entity.getLocationLng()) : null);
    }

    @Override
    public RealmChatMessage toEntity(ChatMessage entity) {
        RealmChatMessage message = new RealmChatMessage().setId(entity.getId()).setText(entity.getText())
                .setSender(entity.getSender())
                .setReceiver(entity.getReceiver())
                .setLocalImagePath(entity.getLocalImagePath())
                .setCreatedTime(entity.getCreatedTime());
        if (entity.getPlace() != null) {
            return message.setLocationLat(entity.getPlace().latitude)
                    .setLocationLng(entity.getPlace().longitude);
        }
        return message;
    }
}
