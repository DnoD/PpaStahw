package com.dnod.ppastahw.service.impl;

import com.dnod.ppastahw.data.ChatMessage;
import com.dnod.ppastahw.service.IClientApi;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;


public final class DefaultClientApiImpl implements IClientApi {

    @Override
    public Observable<List<ChatMessage>> loadChatHistory(String userId) {
        return PublishSubject.create();
    }

    @Override
    public void sendNewMessage(ChatMessage message) {
    }
}
