package com.dnod.ppastahw.service;


import com.dnod.ppastahw.data.ChatMessage;

import java.util.List;

import io.reactivex.Observable;

public interface IClientApi {

    Observable<List<ChatMessage>> loadChatHistory(String userId);

    void sendNewMessage(ChatMessage message);
}
