package com.dnod.ppastahw.service;

import com.dnod.ppastahw.data.ChatMessage;

import java.util.List;

import io.reactivex.Observable;

public interface IChatProvider {

    Observable<List<ChatMessage>> loadChat(String userId);

    Observable<ChatMessage> handleMessage();

    void send(ChatMessage message);

    void release();
}