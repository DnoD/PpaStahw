package com.dnod.ppastahw.service.impl.storage;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class RealmChatMessage extends RealmObject {
    @PrimaryKey
    private String id;
    private String sender;
    private String receiver;
    private String text;
    private String localImagePath;
    private long createdTime;
    private Double locationLat;
    private Double locationLng;

    String getReceiver() {
        return receiver;
    }

    RealmChatMessage setReceiver(String receiver) {
        this.receiver = receiver;
        return this;
    }

    String getLocalImagePath() {
        return localImagePath;
    }

    RealmChatMessage setLocalImagePath(String localImagePath) {
        this.localImagePath = localImagePath;
        return this;
    }

    Double getLocationLat() {
        return locationLat;
    }

    RealmChatMessage setLocationLat(double locationLat) {
        this.locationLat = locationLat;
        return this;
    }

    Double getLocationLng() {
        return locationLng;
    }

    RealmChatMessage setLocationLng(double locationLng) {
        this.locationLng = locationLng;
        return this;
    }

    long getCreatedTime() {
        return createdTime;
    }

    RealmChatMessage setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
        return this;
    }

    String getSender() {
        return sender;
    }

    RealmChatMessage setSender(String sender) {
        this.sender = sender;
        return this;
    }

    String getId() {
        return id;
    }

    RealmChatMessage setId(String id) {
        this.id = id;
        return this;
    }

    String getText() {
        return text;
    }

    RealmChatMessage setText(String text) {
        this.text = text;
        return this;
    }
}
