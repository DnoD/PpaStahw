package com.dnod.ppastahw.service.impl;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.DrawableTypeRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.cache.ExternalCacheDiskCacheFactory;
import com.bumptech.glide.load.engine.cache.MemorySizeCalculator;
import com.dnod.ppastahw.service.IImageLoader;

import java.io.File;

import javax.inject.Inject;

/**
 * This is an ImageLoader using Glide library, for more info: https://github.com/bumptech/glide
 */
public final class GlideImageLoaderImpl implements IImageLoader {

    private final Glide glide;
    private final RequestManager requestManager;

    @Inject
    public GlideImageLoaderImpl(Context context) {
        glide = Glide.get(context);
        MemorySizeCalculator calculator = new MemorySizeCalculator(context);
        int defaultMemoryCacheSize = calculator.getMemoryCacheSize();
        new GlideBuilder(context).setDiskCache(new ExternalCacheDiskCacheFactory(context, defaultMemoryCacheSize));
        requestManager = Glide.with(context);
    }

    @Override
    public void displayImage(ImageView view, final IImageLoader.LoadingBuilder loadingBuilder) {
        Glide.clear(view);
        DrawableTypeRequest request = (loadingBuilder.getUri() != null) ? requestManager.load(new File(loadingBuilder.getUri().getPath())) :
                requestManager.load(loadingBuilder.getUrl());
        if (loadingBuilder.getPlaceHolder() != INVALID_RES_ID) {
            request.placeholder(loadingBuilder.getPlaceHolder());
        }
        if (loadingBuilder.getOutputBounds() != null) {
            request.override(loadingBuilder.getOutputBounds().getWidth(),
                    loadingBuilder.getOutputBounds().getHeight());
        }
        request.fitCenter().crossFade().into(view);
    }
}
