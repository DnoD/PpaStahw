package com.dnod.ppastahw.service.impl.storage;

/**
 * This is a ChatMessage table representation
 */

public enum TableChatMessage {
    ID("id"),
    SENDER("sender"),
    RECEIVER("receiver"),
    TEXT("text"),
    LOCAL_IMAGE_PATH("localImagePath"),
    CREATED_TIME("createdTime"),
    LOCATION_LAT("locationLat"),
    LOCATION_LNG("locationLng");

    private String columnName;

    TableChatMessage(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnName() {
        return columnName;
    }

    public static String getTableName() {
        return "classes";
    }
}
