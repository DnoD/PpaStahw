package com.dnod.ppastahw.service.impl;

import android.util.Log;

import com.dnod.ppastahw.data.ChatMessage;
import com.dnod.ppastahw.service.IChatProvider;
import com.dnod.ppastahw.service.IClientApi;
import com.dnod.ppastahw.service.IDataStorage;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public final class DefaultChatProvider implements IChatProvider {

    private final IClientApi mClientApi;
    private final IDataStorage mDataStorage;
    private final PublishSubject<ChatMessage> mMessagePublish;
    private final PublishSubject<List<ChatMessage>> mHistoryPublish;
    private final IDataStorage.Observer mDataStorageObserver;

    @Inject
    public DefaultChatProvider(IDataStorage dataStorage, IClientApi clientApi) {
        this.mDataStorage = dataStorage;
        this.mClientApi = clientApi;
        this.mMessagePublish = PublishSubject.create();
        this.mHistoryPublish = PublishSubject.create();
        this.mDataStorageObserver = new IDataStorage.Observer() {
            @Override
            public void onNewMessage(ChatMessage message) {
                mMessagePublish.onNext(message);
            }
        };
    }

    @Override
    public Observable<List<ChatMessage>> loadChat(final String userId) {
        mDataStorage.registerDataObserver(userId, mDataStorageObserver);
        mClientApi.loadChatHistory(userId).subscribe(new Consumer<List<ChatMessage>>() {
            @Override
            public void accept(List<ChatMessage> messages) throws Exception {
                mDataStorage.saveChatHistory(messages);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                Log.d("", "");
            }
        });
        Schedulers.io().createWorker().schedule(new Runnable() {
            @Override
            public void run() {
                List<ChatMessage> history = mDataStorage.getChatHistory(userId);
                mHistoryPublish.onNext(history);
            }
        });
        return mHistoryPublish;
    }

    @Override
    public Observable<ChatMessage> handleMessage() {
        return mMessagePublish;
    }

    @Override
    public void send(ChatMessage message) {
        mClientApi.sendNewMessage(message);
        mDataStorage.saveMessage(message);
    }

    @Override
    public void release() {
        mDataStorage.unregisterDataObserver(mDataStorageObserver);
    }
}
