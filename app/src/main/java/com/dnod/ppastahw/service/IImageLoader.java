package com.dnod.ppastahw.service;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;

public interface IImageLoader {
    int INVALID_RES_ID = 0;

    void displayImage(ImageView view, LoadingBuilder loadingBuilder);

    final class LoadingBuilder {
        String url;
        Uri uri;
        ImageBounds outputBounds;
        int placeHolder;

        public LoadingBuilder setUrl(String url) {
            this.url = url;
            return this;
        }

        public LoadingBuilder setUri(Uri uri) {
            this.uri = uri;
            return this;
        }

        public LoadingBuilder setPlaceHolder(int placeHolder) {
            this.placeHolder = placeHolder;
            return this;
        }

        public LoadingBuilder setOutputBounds(ImageBounds outputBounds) {
            this.outputBounds = outputBounds;
            return this;
        }

        public String getUrl() {
            return url;
        }

        public Uri getUri() {
            return uri;
        }

        public int getPlaceHolder() {
            return placeHolder;
        }

        public ImageBounds getOutputBounds() {
            return outputBounds;
        }
    }

    final class ImageBounds {
        protected int height = -1;
        protected int width = -1;

        public ImageBounds( int width, int height) {
            this.height = height;
            this.width = width;
        }

        public int getHeight() {
            return height;
        }

        public int getWidth() {
            return width;
        }
    }
}
