package com.dnod.ppastahw.service.impl;

import android.content.Context;

import com.dnod.ppastahw.data.ChatMessage;
import com.dnod.ppastahw.service.IDataStorage;
import com.dnod.ppastahw.service.impl.storage.ChatMessageMarshaller;
import com.dnod.ppastahw.service.impl.storage.RealmChatMessage;
import com.dnod.ppastahw.service.impl.storage.TableChatMessage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.realm.OrderedCollectionChangeSet;
import io.realm.OrderedRealmCollectionChangeListener;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

public final class RealmDataStorageImpl implements IDataStorage {
    private final static int SCHEMA_VERSION = 1;

    private final Map<Observer, OrderedRealmCollectionChangeListener<RealmResults<RealmChatMessage>>> mListenersMap = new HashMap<>();
    private final RealmResults<RealmChatMessage> mChatData;

    @Inject
    public RealmDataStorageImpl(Context context) {
        Realm.init(context);
        // The RealmConfiguration is created using the builder pattern.
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("ppastahw.realm")
                .schemaVersion(SCHEMA_VERSION)
                .build();
        Realm.setDefaultConfiguration(config);
        Realm realmInstance = Realm.getDefaultInstance();
        mChatData = realmInstance.where(RealmChatMessage.class)
                .findAllSorted(TableChatMessage.CREATED_TIME.getColumnName(), Sort.DESCENDING);
    }

    @Override
    public void saveChatHistory(List<ChatMessage> history) {
        Realm realmInstance = Realm.getDefaultInstance();
        final List<RealmChatMessage> messages = ChatMessageMarshaller.getInstance().toEntities(history);
        realmInstance.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(messages);
            }
        });
        realmInstance.close();
    }

    @Override
    public List<ChatMessage> getChatHistory(String userId) {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<RealmChatMessage> query = realm.where(RealmChatMessage.class)
                .equalTo(TableChatMessage.RECEIVER.getColumnName(), userId)
                .or().equalTo(TableChatMessage.SENDER.getColumnName(), userId);
        RealmResults<RealmChatMessage> items =
                query.findAllSorted(TableChatMessage.CREATED_TIME.getColumnName(), Sort.DESCENDING);
        List<ChatMessage> result = ChatMessageMarshaller.getInstance().fromEntities(items);
        realm.close();
        return result;
    }

    @Override
    public void saveMessage(ChatMessage message) {
        saveObject(ChatMessageMarshaller.getInstance().toEntity(message));
    }

    @Override
    public void registerDataObserver(final String userId, Observer observer) {
        OrderedRealmCollectionChangeListener listener = new OrderedRealmCollectionChangeListener<RealmResults<RealmChatMessage>>() {
            @Override
            public void onChange(RealmResults<RealmChatMessage> collection, OrderedCollectionChangeSet changeSet) {
                // `null`  means the async query returns the first time.
                if (changeSet == null) {
                    return;
                }
                for (Map.Entry<Observer, OrderedRealmCollectionChangeListener<RealmResults<RealmChatMessage>>> observerEntry : mListenersMap.entrySet()) {
                    if (observerEntry.getValue() == this) {
                        int[] insertions = changeSet.getInsertions();
                        ChatMessage chatMessage;
                        for (int i = insertions.length - 1; i >= 0; i--) {
                            chatMessage = ChatMessageMarshaller.getInstance()
                                    .fromEntity(collection.get(insertions[i]));
                            if (chatMessage.getSender().equals(userId) ||
                                    chatMessage.getReceiver().equals(userId)) {
                                observerEntry.getKey()
                                        .onNewMessage(chatMessage);
                            }
                        }
                        return;
                    }
                }
            }
        };
        mListenersMap.put(observer, listener);
        mChatData.addChangeListener(listener);
    }

    @Override
    public void unregisterDataObserver(Observer observer) {
        mChatData.removeChangeListener(mListenersMap.get(observer));
    }

    private void saveObject(final RealmObject object) {
        Realm realmInstance = Realm.getDefaultInstance();
        realmInstance.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(object);
            }
        });
        realmInstance.close();
    }
}
