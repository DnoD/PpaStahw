package com.dnod.ppastahw.chat;

import com.dnod.ppastahw.data.ChatMessage;
import com.dnod.ppastahw.data.User;
import com.dnod.ppastahw.service.IChatProvider;
import com.dnod.ppastahw.service.IImageLoader;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public final class ChatPresenter implements ChatContract.Presenter {
    private static final int BOT_MESSAGE_INTERVAL_IN_SECONDS = 3;

    private final IChatProvider mChatProvider;
    private final ChatContract.View mView;
    private final IImageLoader mImageLoader;
    private final CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private User mUser;

    @Inject
    public ChatPresenter(IChatProvider provider, IImageLoader imageLoader, ChatContract.View view) {
        this.mChatProvider = provider;
        this.mView = view;
        this.mImageLoader = imageLoader;
    }

    @Inject
    void setup() {
        mView.setPresenter(this);
        mCompositeDisposable.add(mChatProvider.handleMessage().observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<ChatMessage>() {
                    @Override
                    public void accept(ChatMessage message) throws Exception {
                        mView.showMessage(message);
                    }
                }));
        mView.setImageLoader(mImageLoader);
    }

    @Override
    public void loadChat() throws NotAuthorizedException {
        if (!validateUser()) throw new NotAuthorizedException();
        mCompositeDisposable.add(mChatProvider.loadChat(mUser.getId())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<ChatMessage>>() {
                    @Override
                    public void accept(List<ChatMessage> messages) throws Exception {
                        mView.showChat(messages);
                        runBot();
                    }
                }));
    }

    @Override
    public void addNewMessage(ChatMessage message) throws NotAuthorizedException {
        if (!validateUser()) throw new NotAuthorizedException();
        mChatProvider.send(message.setId(UUID.randomUUID().toString())
                .setSender(mUser.getId()).setReceiver("BOT"));

    }

    @Override
    public void saveUser(String id) {
        mUser = new User().setId(id);
        mView.setUser(mUser);
    }

    @Override
    public void release() {
        mChatProvider.release();
        mCompositeDisposable.clear();
        mUser = null;
    }

    private boolean validateUser() {
        if (mUser == null) {
            mView.showNewUserDialog();
            return false;
        }
        return true;
    }

    private void runBot() {
        mCompositeDisposable.add(Observable.interval(BOT_MESSAGE_INTERVAL_IN_SECONDS, TimeUnit.SECONDS)
                .observeOn(Schedulers.io())
                .subscribe(new Consumer<Long>() {
                    @Override
                    public void accept(Long aLong) throws Exception {
                        mChatProvider.send(new ChatMessage().setSender("BOT")
                                .setReceiver(mUser.getId())
                                .setCreatedTime(System.currentTimeMillis())
                                .setText("Hello " + mUser.getId() + ", my name is BOT =)")
                                .setId(UUID.randomUUID().toString()));
                    }
                }));
    }
}
