package com.dnod.ppastahw.chat;

import com.dnod.ppastahw.BasePresenter;
import com.dnod.ppastahw.BaseView;
import com.dnod.ppastahw.data.ChatMessage;
import com.dnod.ppastahw.data.User;
import com.dnod.ppastahw.service.IImageLoader;

import java.util.List;

/**
 * This is a interface that describes the Contract(behaviour) between view and presenter
 */
public interface ChatContract {

    interface Presenter extends BasePresenter {
        void loadChat() throws NotAuthorizedException ;

        void saveUser(String id);

        void addNewMessage(ChatMessage message) throws NotAuthorizedException ;

        void release();
    }

    interface View extends BaseView<Presenter> {
        void setImageLoader(IImageLoader imageLoader);

        void showChat(List<ChatMessage> messages);

        void showMessage(ChatMessage message);

        void showNewUserDialog();

        void setUser(User user);
    }
}
