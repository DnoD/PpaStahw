package com.dnod.ppastahw.chat;

import com.dnod.ppastahw.service.IChatProvider;
import com.dnod.ppastahw.service.impl.DefaultChatProvider;

import dagger.Module;
import dagger.Provides;

@Module
public class ChatModule {

    private final ChatContract.View mView;

    public ChatModule(ChatContract.View view) {
        mView = view;
    }

    @Provides
    @ChatScope
    ChatContract.View provideChatView() {
        return mView;
    }

    @Provides
    @ChatScope
    IChatProvider provideChatProvider(DefaultChatProvider provider) {
        return provider;
    }
}
