package com.dnod.ppastahw.chat;

import android.databinding.DataBindingUtil;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.dnod.ppastahw.AppController;
import com.dnod.ppastahw.R;
import com.dnod.ppastahw.databinding.ActivityChatBinding;

import javax.inject.Inject;

import static com.google.common.base.Preconditions.checkNotNull;

public class ChatActivity extends AppCompatActivity {

    @Inject
    ChatPresenter chatPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityChatBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_chat);
        FragmentManager fragmentManager = getSupportFragmentManager();
        ChatFragment chatFragment =
                (ChatFragment) fragmentManager.findFragmentById(binding.contentFrame.getId());
        if (chatFragment == null) {
            // Create the fragment
            chatFragment = ChatFragment.newInstance();
            checkNotNull(fragmentManager);
            checkNotNull(chatFragment);
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.add(binding.contentFrame.getId(), chatFragment);
            transaction.commit();
        }
        AppController.getAppComponent().joinChatComponent(new ChatModule(chatFragment)).inject(this);
    }
}
