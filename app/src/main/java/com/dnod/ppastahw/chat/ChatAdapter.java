package com.dnod.ppastahw.chat;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dnod.ppastahw.R;
import com.dnod.ppastahw.data.ChatMessage;
import com.dnod.ppastahw.databinding.ItemIncomingMessageBinding;
import com.dnod.ppastahw.databinding.ItemOutgoingMessageBinding;
import com.dnod.ppastahw.service.IImageLoader;
import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public final class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    interface Listener {
        void onPlaceClick(LatLng latLng);

        void onImageClick(String imagePath);
    }

    private enum HolderType {
        INCOMING, OUTGOING
    }

    private final LayoutInflater mLayoutInflater;
    private final ArrayList<ChatMessage> mMessages;
    private final Listener mListener;
    private final IImageLoader mImageLoader;
    private final int mImagePreviewSize;
    private String mOwnerId;

    ChatAdapter(Context context, IImageLoader mImageLoader, Listener mListener) {
        this.mLayoutInflater = LayoutInflater.from(context);
        this.mImageLoader = mImageLoader;
        this.mListener = mListener;
        mMessages = new ArrayList<>();
        mImagePreviewSize = (int) context.getResources().getDimension(R.dimen.message_image_size);
        setHasStableIds(true);
    }

    ChatAdapter setOwnerId(String ownerId) {
        this.mOwnerId = ownerId;
        return this;
    }

    void replaceAll(@NonNull List<ChatMessage> messages) {
        this.mMessages.clear();
        this.mMessages.trimToSize();
        this.mMessages.addAll(messages);
        notifyDataSetChanged();
    }

    void add(ChatMessage message) {
        mMessages.add(0, message);
        notifyItemInserted(0);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (HolderType.values()[viewType]) {
            case INCOMING:
                return new IncomingMessageHolder((ItemIncomingMessageBinding) DataBindingUtil.inflate(
                        mLayoutInflater, R.layout.item_incoming_message, parent, false
                ));
            case OUTGOING:
                return new OutgoingMessageHolder((ItemOutgoingMessageBinding) DataBindingUtil.inflate(
                        mLayoutInflater, R.layout.item_outgoing_message, parent, false
                ));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ChatMessage chatMessage = mMessages.get(position);
        if (holder instanceof IncomingMessageHolder) {
            IncomingMessageHolder messageHolder = (IncomingMessageHolder) holder;
            messageHolder.binding.setModel(chatMessage);
            if (chatMessage.getLocalImagePath() != null) {
                mImageLoader.displayImage(messageHolder.binding.image, new IImageLoader.LoadingBuilder()
                        .setUri(Uri.parse(chatMessage.getLocalImagePath()))
                        .setOutputBounds(new IImageLoader.ImageBounds(mImagePreviewSize, mImagePreviewSize)));
            }
        } else if (holder instanceof OutgoingMessageHolder) {
            OutgoingMessageHolder messageHolder = (OutgoingMessageHolder) holder;
            messageHolder.binding.setModel(chatMessage);
            if (chatMessage.getLocalImagePath() != null) {
                mImageLoader.displayImage(messageHolder.binding.image, new IImageLoader.LoadingBuilder()
                        .setUri(Uri.fromFile(new File(chatMessage.getLocalImagePath())))
                        .setOutputBounds(new IImageLoader.ImageBounds(mImagePreviewSize, mImagePreviewSize)));
            }
        }
    }

    @Override
    public int getItemCount() {
        return mMessages.size();
    }

    @Override
    public long getItemId(int position) {
        return mMessages.get(position).getId().hashCode();
    }

    @Override
    public int getItemViewType(int position) {
        return mMessages.get(position).getSender().equals(mOwnerId) ?
                HolderType.OUTGOING.ordinal() : HolderType.INCOMING.ordinal();
    }

    final class IncomingMessageHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemIncomingMessageBinding binding;

        IncomingMessageHolder(ItemIncomingMessageBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.locationIcon.setOnClickListener(this);
            this.binding.image.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.location_icon:
                    mListener.onPlaceClick(mMessages.get(getAdapterPosition()).getPlace());
                    break;
                case R.id.image:
                    mListener.onImageClick(mMessages.get(getAdapterPosition()).getLocalImagePath());
                    break;
            }
        }
    }

    final class OutgoingMessageHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemOutgoingMessageBinding binding;

        OutgoingMessageHolder(ItemOutgoingMessageBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.image.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.location_icon:
                    mListener.onPlaceClick(mMessages.get(getAdapterPosition()).getPlace());
                    break;
                case R.id.image:
                    mListener.onImageClick(mMessages.get(getAdapterPosition()).getLocalImagePath());
                    break;
            }
        }
    }
}
