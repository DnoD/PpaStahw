package com.dnod.ppastahw.chat;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.dnod.ppastahw.R;
import com.dnod.ppastahw.data.ChatMessage;
import com.dnod.ppastahw.data.User;
import com.dnod.ppastahw.databinding.FragmentChatBinding;
import com.dnod.ppastahw.databinding.ViewNicknameFieldBinding;
import com.dnod.ppastahw.service.IImageLoader;
import com.dnod.ppastahw.utils.Constants;
import com.dnod.ppastahw.utils.IntentUtils;
import com.dnod.ppastahw.utils.VerticalSpaceItemDecoration;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ChatFragment extends Fragment implements ChatContract.View {

    public static ChatFragment newInstance() {
        return new ChatFragment();
    }

    private FragmentChatBinding mBinding;
    private ChatAdapter mAdapter;
    private ChatContract.Presenter mChatPresenter;
    private String mImagePath;
    private LatLng mPlace;
    private IImageLoader mImageLoader;

    private final ChatAdapter.Listener mAdapterListener = new ChatAdapter.Listener() {
        @Override
        public void onPlaceClick(LatLng latLng) {
            startActivity(IntentUtils.createGoogleMpasIntent(latLng));
        }

        @Override
        public void onImageClick(String imagePath) {
            Snackbar.make(mBinding.getRoot(), R.string.cooming_soon, Snackbar.LENGTH_SHORT).show();
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat, container, false);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        manager.setReverseLayout(true);
        mBinding.chat.setLayoutManager(manager);
        mBinding.chat.setHasFixedSize(true);
        mBinding.chat.addItemDecoration(
                new VerticalSpaceItemDecoration((int) getResources().getDimension(R.dimen.padding_small)));
        mAdapter = new ChatAdapter(getContext(), mImageLoader, mAdapterListener);
        mBinding.chat.setAdapter(mAdapter);
        mBinding.btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
            }
        });
        mBinding.messageText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateSendButton();
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        mBinding.messageText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    sendMessage();
                    handled = true;
                }
                return handled;
            }
        });
        mBinding.btnPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkForLocationPermission()) {
                    showPlacePicker();
                }
            }
        });
        mBinding.btnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkForReadStoragePermission()) {
                    startActivityForResult(IntentUtils.createImagePickIntent(), Constants.PICK_IMAGE_REQUEST);
                }
            }
        });
        return mBinding.getRoot();
    }

    private void validateSendButton() {
        boolean enabled = !TextUtils.isEmpty(mBinding.messageText.getText()) ||
                mPlace != null || mImagePath != null;
        mBinding.btnSend.setEnabled(enabled);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadChat();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mChatPresenter.release();
    }

    @Override
    public void setUser(User user) {
        mAdapter.setOwnerId(user.getId());
    }

    @Override
    public void setImageLoader(IImageLoader imageLoader) {
        mImageLoader = imageLoader;
    }

    @Override
    public void setPresenter(ChatContract.Presenter presenter) {
        mChatPresenter = presenter;
    }

    @Override
    public void showChat(List<ChatMessage> messages) {
        mAdapter.replaceAll(messages);
    }

    @Override
    public void showMessage(ChatMessage message) {
        mAdapter.add(message);
    }

    @Override
    public void showNewUserDialog() {
        final ViewNicknameFieldBinding nicknameFieldBinding = DataBindingUtil.inflate(
                LayoutInflater.from(getActivity()), R.layout.view_nickname_field, (ViewGroup) mBinding.getRoot(), false
        );
        new AlertDialog.Builder(getActivity())
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (TextUtils.isEmpty(nicknameFieldBinding.nickname.getText())) {
                            mChatPresenter.saveUser(UUID.randomUUID().toString());
                        } else {
                            mChatPresenter.saveUser(nicknameFieldBinding.nickname.getText().toString());
                        }
                        loadChat();
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .setView(nicknameFieldBinding.getRoot())
                .setTitle(R.string.title_dialog_user_name)
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constants.FINE_LOCATION_PERMISSION_REQUEST:
                for (Integer result : grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        Snackbar.make(mBinding.getRoot(), R.string.error_permission_denied, Snackbar.LENGTH_LONG).show();
                        return;
                    }
                }
                showPlacePicker();
                break;
            case Constants.READ_STORAGE_PERMISSION_REQUEST:
                for (Integer result : grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        Snackbar.make(mBinding.getRoot(), R.string.error_permission_denied, Snackbar.LENGTH_LONG).show();
                        return;
                    }
                }
                startActivityForResult(IntentUtils.createImagePickIntent(), Constants.PICK_IMAGE_REQUEST);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Constants.PLACE_PICKER_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    Place place = PlacePicker.getPlace(getContext(), data);
                    mPlace = place.getLatLng();
                }
                break;
            case Constants.PICK_IMAGE_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    if (cursor != null) {
                        cursor.moveToFirst();
                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        mImagePath = cursor.getString(columnIndex);
                        cursor.close();
                    } else {
                        Snackbar.make(mBinding.getRoot(), R.string.error_pick_image, Snackbar.LENGTH_SHORT).show();
                    }

                }
                break;
        }
    }

    private boolean checkForReadStoragePermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            //Check Location service permissions and request if needed
            List<String> permissions = new ArrayList<>();
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }
            if (permissions.size() != 0) {
                ActivityCompat.requestPermissions(getActivity(), permissions.toArray(new String[permissions.size()]),
                        Constants.FINE_LOCATION_PERMISSION_REQUEST);
                return false;
            }
        }
        return true;
    }

    private boolean checkForLocationPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            //Check Location service permissions and request if needed
            List<String> permissions = new ArrayList<>();
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }
            if (permissions.size() != 0) {
                ActivityCompat.requestPermissions(getActivity(), permissions.toArray(new String[permissions.size()]),
                        Constants.FINE_LOCATION_PERMISSION_REQUEST);
                return false;
            }
        }
        return true;
    }

    private void sendMessage() {
        try {
            mChatPresenter.addNewMessage(new ChatMessage()
                    .setText(mBinding.messageText.getText().toString())
                    .setCreatedTime(System.currentTimeMillis())
                    .setLocalImagePath(mImagePath)
                    .setPlace(mPlace));
        } catch (NotAuthorizedException e) {
            e.printStackTrace();
            return;
        }
        mPlace = null;
        mImagePath = null;
        mBinding.messageText.setText("");
        mBinding.chat.smoothScrollToPosition(0);
    }

    private void showPlacePicker() {
        try {
            PlacePicker.IntentBuilder intentBuilder =
                    new PlacePicker.IntentBuilder();
            if (mPlace != null) {
                intentBuilder.setLatLngBounds(LatLngBounds.builder().include(mPlace).build());
            }
            Intent intent = intentBuilder.build(getActivity());
            startActivityForResult(intent, Constants.PLACE_PICKER_REQUEST);

        } catch (Exception e) {
            Snackbar.make(mBinding.getRoot(), R.string.error_google_play_services,
                    Snackbar.LENGTH_LONG).show();
        }
    }

    private void loadChat() {
        try {
            mChatPresenter.loadChat();
        } catch (NotAuthorizedException e) {
            e.printStackTrace();
        }
    }

}
