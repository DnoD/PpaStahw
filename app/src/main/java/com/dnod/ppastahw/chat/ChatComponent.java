package com.dnod.ppastahw.chat;

import com.dnod.ppastahw.ApplicationComponent;

import dagger.Component;
import dagger.Subcomponent;

@ChatScope
@Subcomponent(modules = ChatModule.class)
public interface ChatComponent {

    void inject(ChatActivity activity);
}
