package com.dnod.ppastahw;

import com.dnod.ppastahw.chat.ChatComponent;
import com.dnod.ppastahw.chat.ChatModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    ChatComponent joinChatComponent(ChatModule chatModule);
}
