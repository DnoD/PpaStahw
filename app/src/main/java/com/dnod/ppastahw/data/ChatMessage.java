package com.dnod.ppastahw.data;

import com.google.android.gms.maps.model.LatLng;

public class ChatMessage {
    private String id;
    private String sender;
    private String receiver;
    private String text;
    private String localImagePath;
    private long createdTime;
    private LatLng place;

    public String getReceiver() {
        return receiver;
    }

    public ChatMessage setReceiver(String receiver) {
        this.receiver = receiver;
        return this;
    }

    public String getLocalImagePath() {
        return localImagePath;
    }

    public ChatMessage setLocalImagePath(String localImagePath) {
        this.localImagePath = localImagePath;
        return this;
    }

    public LatLng getPlace() {
        return place;
    }

    public ChatMessage setPlace(LatLng place) {
        this.place = place;
        return this;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public ChatMessage setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
        return this;
    }

    public String getSender() {
        return sender;
    }

    public ChatMessage setSender(String sender) {
        this.sender = sender;
        return this;
    }

    public String getId() {
        return id;
    }

    public ChatMessage setId(String id) {
        this.id = id;
        return this;
    }

    public String getText() {
        return text;
    }

    public ChatMessage setText(String text) {
        this.text = text;
        return this;
    }
}
