package com.dnod.ppastahw.data;

public final class User {
    private String id;

    public String getId() {
        return id;
    }

    public User setId(String id) {
        this.id = id;
        return this;
    }
}
