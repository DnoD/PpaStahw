package com.dnod.ppastahw;

import android.content.Context;

import com.dnod.ppastahw.service.IClientApi;
import com.dnod.ppastahw.service.IDataStorage;
import com.dnod.ppastahw.service.IImageLoader;
import com.dnod.ppastahw.service.impl.DefaultClientApiImpl;
import com.dnod.ppastahw.service.impl.GlideImageLoaderImpl;
import com.dnod.ppastahw.service.impl.RealmDataStorageImpl;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private final Context mContext;

    @Inject
    public ApplicationModule(Context mContext) {
        this.mContext = mContext;
    }

    @Provides
    Context provideContext() {
        return mContext;
    }

    @Provides
    @Singleton
    IClientApi provideClientApi() {
        return new DefaultClientApiImpl();
    }

    @Provides
    @Singleton
    IDataStorage provideDataStorage(RealmDataStorageImpl dataStorage) {
        return dataStorage;
    }

    @Provides
    @Singleton
    IImageLoader provideImageLoader(GlideImageLoaderImpl imageLoader) {
        return imageLoader;
    }
}
