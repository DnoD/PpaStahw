package com.dnod.ppastahw;


import android.app.Application;

public final class AppController extends Application {

    private static AppController sInstance;

    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();
    }

    public static AppController getInstance() {
        return sInstance;
    }

    public static ApplicationComponent getAppComponent() {
        return sInstance.mApplicationComponent;
    }
}
