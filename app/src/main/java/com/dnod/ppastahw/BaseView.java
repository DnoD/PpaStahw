package com.dnod.ppastahw;


public interface BaseView<T> {

    void setPresenter(T presenter);
}
